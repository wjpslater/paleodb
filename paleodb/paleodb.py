#!/usr/bin/env python3

import sys
import sqlite3
import json
import click
import requests
from . import __version__ as version


def from_url(url, use_pbdb=False):
    try:
        if use_pbdb:
            url += "&vocab=pbdb"
        print(f"Sending request: {url}")
        with requests.get(url, stream=True) as resp:

            content_length = resp.headers.get('content-length')

            if content_length:
                total_kbs = content_length / 1024
            else:
                total_kbs = 'unknown'
            chunks = []
            for data in resp.iter_content(chunk_size=1024):
                chunks.append(data)
                sys.stdout.write("\r" + " " * 80)
                sys.stdout.write(f"\r{len(chunks)} / {total_kbs} KB")
                sys.stdout.flush()
            sys.stdout.write("\r" + " " * 80)
            sys.stdout.write("\r")
            sys.stdout.flush()
            print("done")

            return check_data(json.loads(b''.join(chunks)))

    except Exception as e:
        raise Exception(
            "Unable to download paleodb data from supplied url: {e}")


def from_file(f):
    try:
        return check_data(json.loads(f.read()))
    except Exception as e:
        raise Exception(f"Unable to load json data from {f.name}: {e}")


def check_data(data):
    if not {'records', 'records_returned'}.issubset(set(data)):
        raise Exception("Doesn't look like a valid json data set")

    if len(data['records']) == 0:
        raise Exception("No records in data set")

    if len(data['records']) != data['records_returned']:
        print(f"Number of records {len(data['records'])}"
              f"does not match header {data['records_returned']}")

    print(f"{data['records_returned']} records found in data file")
    return data


def build_db(db_fname, data, tab_name="occurences", append=False):
    print(f"creating db in {db_fname}")

    db = sqlite3.connect(db_fname)
    curs = db.cursor()

    def map_type(v):
        if isinstance(v, str):
            return 'varchar'
        if isinstance(v, float):
            return 'numeric'
        if isinstance(v, int):
            return 'integer'
        return 'varchar'

    records = data['records']

    cols = sorted(list(set([]).union(*data['records'])))
    print(f"Found {len(cols)} cols: {cols}")

    row0 = records[0]
    col_str = ', '.join(f'"{k}" {map_type(row0.get(k))}' for k in cols)
    tab_sql = f'create table "{tab_name}" ({col_str})'
    ins_sql = f"""
            insert into {tab_name} (
                    {', '.join([f'"{c}"' for c in cols])}
                ) values (
                    {', '.join([':'+c for c in cols])}
                )
            """
    try:
        curs.execute(tab_sql)
    except sqlite3.OperationalError as e:
        if not append and 'already exists' in str(e):
            curs.execute(f'drop table "{tab_name}"')
            curs.execute(tab_sql)
        elif append and 'already exists' in str(e):
            pass
        else:
            raise Exception(f"unable to create table {tab_name}: {e}")

    print(f"loading data into table {tab_name}")
    for row in records:
        try:
            curs.execute(ins_sql, {c: row.get(c) for c in cols})
        except Exception as e:
            print(f"Error inserting {row} : {e}")

    db.commit()
    print(f"loaded {len(records)} successfully")


@click.command("paleodb")
@click.option("-u", "--url", help="paleod db url to download from")
@click.option("-f", "--data-file", help="paleodb json file to load")
@click.option("-d", "--db-path", help="database to load the data into",
              default="paleodb.sqlite")
@click.option("-t", "--table", help="table to load the data into",
              default="occurences")
@click.option("-a", "--append/--no-append", help="append to table if exists",
              default=False)
@click.option("--long-cols", "use_pbdb", help="Use pdbd vocab for columns names",
              is_flag=True)
@click.version_option(version=version)
def main(url, data_file, db_path, table, append, use_pbdb):
    if url:
        data = from_url(url, use_pbdb)
    elif data_file:
        with open(data_file, "r") as f:
            data = from_file(f)
    else:
        raise click.UsageError("url or data-file must be specified")

    build_db(db_path, data, table, append)


if __name__ == '__main__':
    main()
