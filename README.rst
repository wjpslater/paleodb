paleodb
#############################

A super simple library for downloading from paleodb.org and stuffing in an SQLite db.

install
-------

Clone from here and pip install the local package. 

You will need to have sqlite3 pacakge installed if you dont

.. code-block:: shell

    $ python3 -m venv test.venv
    $ . test.venv/bin/activate
    $ git clone git@gitlab.com:wjpslater/paleodb.git
    $ pip install -e paleodb
    $ paleodb --help
    Usage: paleodb [OPTIONS]

    Options:
      -u, --url TEXT              paleod db url to download from
      -f, --data-file TEXT        paleodb json file to load
      -d, --db-path TEXT          database to load the data into
      -t, --table TEXT            table to load the data into
      -a, --append / --no-append  append to table if exists
      --long-cols                 Use pdbd vocab for columns names
      --help                      Show this message and exit.


usage
-------
Simple usage as follows

.. code-block:: shell

    $ paleodb --url "https://paleobiodb.org/data1.2/occs/list.json?datainfo&rowcount&taxon_reso=genus&max_ma=55&min_ma=0&cc=ATA,AFR&show=lith,env,timebins" --long-cols
    Sending request: https://paleobiodb.org/data1.2/occs/list.json?datainfo&rowcount&taxon_reso=genus&max_ma=55&min_ma=0&cc=ATA,AFR&show=lith,env,timebins&vocab=pbdb
    done
    25696 records found in data file
    creating db in paleo.db
    loading data into table occurences
    loaded 25696 successfully
    $
    $ sqlite3 paleodb.sqlite
    SQLite version 3.22.0 2018-01-22 18:45:57
    Enter ".help" for usage hints.
    sqlite> select count(*) from occurences;
    25696
    sqlite>

License
-------

This code is licensed under the `MIT License`_.

.. _`MIT License`: https://github.com/""/paleodb/blob/master/LICENSE

