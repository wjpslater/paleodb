import sys

from setuptools import setup, find_packages

requires = ["click", "requests"]
tests_requires = ["pytest", "pytest-cache", "pytest-cov"]
lint_requires = ["flake8", "black"]
dev_requires = ["bumpversion"] + requires + tests_requires + lint_requires


setup(
    name="paleodb",
    version="18.10.2.4",
    description="Simple tool to download data from paleodb.org and load into SQLite database",
    long_description="\n\n".join([open("README.rst").read()]),
    license="MIT",
    author="Will Slater",
    author_email="wjpslater@gmail.com ",
    url="https://paleodb.readthedocs.org",
    packages=find_packages(),
    install_requires=requires,
    entry_points={
        "console_scripts": [
            "paleodb = paleodb.paleodb:main"
        ]
    },
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
    extras_require={"test": tests_requires, "dev": dev_requires, "lint": lint_requires},
    python_requires='>=3.6',
)
